This is my M.Sc final year project and its also fellowship project of ICT division.
Name of the project: "Connecting world through the Internet of Things ( IoT) and minimizing the security & privacy risk".

Used Tools:

Server: NodeJs, ExpressJs, RestAPI, MQTT protocol
DataBase: MongoDB
Software: Visual studio code, Mosquitto (MQTT broker), Robomongo for visualize MongoDB as GUI, Arduino, MQTT lense, POSTMan 
Hardware: Arduino UNO, GSM/GPRS SIM900A, ACS712-30A Current sensor Module, 12 power supply, SIM CARD, BULB, Relay Module.

Working Principle:

Take action based on ON/OFF command from browser.
publish action feedback(Electricity available or not, Bulb or cable status,Light ON/OF status ) to server and browser pull these update from server
Arduino Scane the system in loop and publish system updates (Electricity Available or not, Bulb/cable status, light ON/OFF status)