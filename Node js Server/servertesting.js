var http = require('http');
var express = require('express');
var bodyParser = require("body-parser");
var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));
var fs = require('fs');
//...........................MQTT initialization begin
var mqtt_pub_topic="tx";
var mqtt_sub_topic="rx";
const mqtt = require('mqtt')
const client = mqtt.connect('mqtt://182.163.112.205')

client.on('connect', () => {
    client.subscribe(mqtt_sub_topic)
})
//.......................MQTT END
const mongoose = require('mongoose');
myDB = require('./models/mydb');

// Connect to Mongoose
mongoose.connect('mongodb://localhost/smart_home_db', { useMongoClient: true });
var db = mongoose.connection;


app.get('/', function (request, response) {
    response.sendFile(__dirname + "/public/" + "1.html");
    /* fs.readFile('public/home.html',function(error,data)
     {
      response.send(data.toString());
      // console.log(data.toString());
     });*/
    console.log("1.html is sent");
});
app.post('/getStatus', function (req, res) {
    ////////
    db.collection("action_table").findOne({ appliance_id: "light1" }, { _id: false, appliance_id: true, action_command: true, action_status: true }, function (err, result) {
        if (err) throw err;
        else {
            if(result.action_status=="pending")
                {
                    res.json({appliance_id: "light1", status_value:4});
                }
                else
                  {
                    var appliance_id = req.body.appliance_id;
                    db.collection("status_table").findOne({ appliance_id: appliance_id }, { _id: false, appliance_id: true, status_value: true }, function (err, result) {
                        if (err) throw err;
                        else {
                            res.json(result);
                            // console.log(result);
                            // console.log("document retrieve successfully");
                        }
                
                    });
                  }
        }

    });
    /////////
   
});

app.post('/addStatus', (req, res) => {
    var status = req.body;
    console.log(status);
    db.collection("status_table").insertOne(status, function (err, result) {
        if (err) throw err;
        console.log("1 document inserted");

    });
    res.json(status);
});

app.put('/updateStatus/:_appliance_id', (req, res) => {
    var appliance_id = req.params._appliance_id;
    var status = req.body;
    var data = { 'si_no': parseInt(status.si_no), 'appliance_id': status.appliance_id, 'status_value': parseInt(status.status_value) };
    console.log(data);
    db.collection("status_table").updateOne({ appliance_id: appliance_id }, data, function (err, result) {
        if (err) throw err;
        else {
            console.log("1 document updated");
            res.json(data);
        }
    });
});
//...................................................action table. begin.................................
app.post('/getAction', function (req, res) {
    var appliance_id = req.body.appliance_id;
    db.collection("status_table").findOne({ appliance_id: appliance_id }, { _id: false, appliance_id: true, status_value: true }, function (err, result) {
        if (err) throw err;
        else {
            res.json(result);
            console.log(result);
            console.log("document retrieve successfully");
        }

    });
});

app.post('/addAction', (req, res) => {
    var status = req.body;
    console.log(status);
    db.collection("action_table").insertOne(status, function (err, result) {
        if (err) throw err;
        console.log("1 document inserted into action table");

    });
    res.json(status);
});


app.put('/updateAction/:_appliance_id', (req, res) => {
    var appliance_id = req.params._appliance_id;
    var status = req.body;
    var data = { 'si_no': parseInt(status.si_no), 'appliance_id': status.appliance_id, 'action_command': parseInt(status.action_command), 'action_status': status.action_status };
    console.log(data);
    db.collection("action_table").updateOne({ appliance_id: appliance_id }, data, function (err, result) {
        if (err) throw err;
        else {
            console.log("1 document updated at action table");
            res.json(data);
        }
    });
    setTimeout(publish_action, 5);
});
//...................................................action table. end.................................
//.................................MQTT....begin...................
//MQTT subscribe // updated message from device
client.on('message', (topic, message) => {
    var received_message = JSON.parse(message);
    console.log(JSON.stringify(received_message));
      //updating status table
      db.collection("status_table").updateOne({ appliance_id: received_message.appliance_id }, received_message, function (err, result) {
        if (err) throw err;
        else {
            console.log("1 document updated at status table from MQTT");
        }
    });
    //updating action table
    var data = { 'si_no': 1, 'appliance_id': "light1", 'action_command':-1, 'action_status':"done"};
    db.collection("action_table").updateOne({ appliance_id: "light1" }, data, function (err, result) {
        if (err) throw err;
        else {
            console.log("1 document updated at action table from MQTT ACK");
          
        }
    });
  


})
//MQTT publish.........sending updated action from server
var publish_action = function () {
    db.collection("action_table").findOne({ appliance_id: "light1" }, { _id: false, appliance_id: true, action_command: true, action_status: true }, function (err, result) {
        if (err) throw err;
        else {
            if(result.action_status=="pending")
                {
                    var mqtt_pub_data={"appliance_id":result.appliance_id,"action_command":result.action_command};
                    client.publish(mqtt_pub_topic, JSON.stringify(mqtt_pub_data));
                    console.log(result);
                    console.log("document published successfully");
                    setTimeout(publish_action, 30000);
                }
                else
                   setTimeout(publish_action, 120000);
        }

    });
}
setTimeout(publish_action, 1000);
//.............MQTT end.............................................
app.listen(80);

console.log("server running...");
