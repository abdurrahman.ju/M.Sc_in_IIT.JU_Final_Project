const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/smart_home_db',{ useMongoClient: true });
var db = mongoose.connection;

// status_table Schema
const statusSchema = mongoose.Schema({
    si_no:{
        type: Number
    },
	appliance_id:{
		type: String,
		required: true
    },
    status_value:{
        type: Number
    },
	create_date:{
		type: Date,
		default: Date.now
	}
});
// action_table Schema
const actionSchema = mongoose.Schema({
    si_no:{
        type: Number
    },
	appliance_id:{
		type: String,
		required: true
    },
    action_command:{
		type: Number,
		required: true
	},
	action_status:{
		type: String,
		required: true
    },
	create_date:{
		type: Date,
		default: Date.now
	}
});

const status_table = module.exports = mongoose.model('status_table', statusSchema);
const action_table = module.exports = mongoose.model('action_table', actionSchema);
