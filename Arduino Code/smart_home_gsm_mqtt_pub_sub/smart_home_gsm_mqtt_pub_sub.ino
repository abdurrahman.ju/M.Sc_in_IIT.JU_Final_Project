#include <GSM_MQTT.h>

#include <SoftwareSerial.h>
#include <ArduinoJson.h>
#include <EEPROM.h>
#define smart_home_sub_topic "tx"//mart_home_pub_topic"
#define smart_home_pub_topic "rx"//"smart_home_sub_topic"
void executeAction(char currentLightStatus);
String MQTT_HOST = "182.163.112.205";
unsigned char msg_id;
//unsigned char received_message[50]={"0"};
unsigned char nc=0;
String MQTT_PORT = "1883";
SoftwareSerial mySerial(9, 10); // RX, TX

void GSM_MQTT::AutoConnect(void)
{
  connect("qwertyuiop", 0, 0, "", "", 1, 0, 0, 0, "", "");
}
void GSM_MQTT::OnConnect(void)
{
  msg_id= _generateMessageID();
  subscribe(0,msg_id, smart_home_sub_topic, 1);
 // publish(0, 0, 0, msg_id, smart_home_pub_topic, "abdur rahman");

}
void GSM_MQTT::OnMessage(char *Topic, int TopicLength, char *Message, int MessageLength)
{

  //strcpy(received_message,Message);
//...................Json parsing............
 
     StaticJsonBuffer<200> jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(Message);
    if (root.success())
        executeAction(root["action_command"]);
  

       
}
GSM_MQTT MQTT(20);//20 is the keepalive duration in seconds

//.....................................smart home begin..............................
#define lightSwitchingPin 13
#define electricitySensingPin A0

#define eepromAddress_light 0
#define electricityStatusCode 2
#define electricityFlowStatusCode 3
#define lightStatusSensingPin A1
int mVperAmp = 66; // use 100 for 20A Module and 66 for 30A Module

char permanentLightStatus=0;
boolean prevelectricityStatus=0;
boolean prevelectricityFlowStatus=0;
void setup()
{
  // GSM modem should be connected to Harware Serial
  MQTT.begin(); // initialize mqtt:
   pinMode(lightSwitchingPin, OUTPUT);
   pinMode(electricitySensingPin,INPUT);
   analogWrite(electricitySensingPin,0);
   permanentLightStatus=EEPROM.read(eepromAddress_light);
   digitalWrite(lightSwitchingPin, permanentLightStatus);
   
}
void loop()
{
  if(MQTT.available())
  {
    delay(5000);
     systemScane();
  }
 
  MQTT.processing();
     delay(1);
}

void executeAction(char currentLightStatus)
{

   if(permanentLightStatus!=currentLightStatus)
  {
     if(currentLightStatus==1)
        {
           if(isElectricityAvailable())
          {
           //Serial.println("Action Command found>> Light ON");
           digitalWrite(lightSwitchingPin,HIGH);
           delay(1000);
           if(isCurrentFlow())
           {
            permanentLightStatus=currentLightStatus;
            EEPROM.write(eepromAddress_light,permanentLightStatus);
            MQTT.publish(0, 0, 0, msg_id, smart_home_pub_topic, "{\"si_no\":1,\"appliance_id\":\"light1\",\"status_value\":1}" );//ON
           }
           else
           {
            digitalWrite(lightSwitchingPin,LOW);
            MQTT.publish(0, 0, 0, msg_id, smart_home_pub_topic, "{\"si_no\":1,\"appliance_id\":\"light1\",\"status_value\":3}" );//Open circuit
            delay(1000);
           }
            
         }
         else
         {
              MQTT.publish(0, 0, 0, msg_id, smart_home_pub_topic, "{\"si_no\":1,\"appliance_id\":\"light1\",\"status_value\":2}" );//Electricity NOT availabe code
         }
           
         
        }
        if(currentLightStatus==0)
        {
            //Serial.println("Action Command found>>Light OFF");
            digitalWrite(lightSwitchingPin,LOW);
           // delay(1000);
          // if(!isCurrentFlow())
         //  {
             permanentLightStatus=currentLightStatus;
            EEPROM.write(eepromAddress_light,permanentLightStatus);
            MQTT.publish(0, 0, 0, msg_id, smart_home_pub_topic, "{\"si_no\":1,\"appliance_id\":\"light1\",\"status_value\":0}" );
          // }
        }
        
  }  
 }
 


boolean isElectricityAvailable()
{

  int analogData=analogRead(electricitySensingPin);
  
    if(analogData>600)
  return true;
  else
  return false;
}


boolean isCurrentFlow()
{
  double Voltage = 0;
  double VRMS = 0;
  double AmpsRMS = 0;
   Voltage = getVPP();
   VRMS = (Voltage/2.0) *0.707; 
   AmpsRMS = (VRMS * 1000)/mVperAmp;
 float mARMS=(AmpsRMS*1000);
 /*
 char ppma[10];
    dtostrf(mARMS,6,3,ppma);
    char mqtt_json_packet[10];
    strcpy(mqtt_json_packet, "p:");
    strcat(mqtt_json_packet, ppma);
  MQTT.publish(0, 0, 0, msg_id, "my", mqtt_json_packet);
  */
 if(mARMS>200)
 return true;
 else
 return false;
}
float getVPP()
{
  float result;
  
  int readValue;             //value read from the sensor
  int maxValue = 0;          // store max value here
  int minValue = 1024;          // store min value here
  
   uint32_t start_time = millis();
   while((millis()-start_time) < 1000) //sample for 1 Sec
   {
       readValue = analogRead(lightStatusSensingPin);
       // see if you have a new maxValue
       if (readValue > maxValue) 
       {
           //record the maximum sensor value
           maxValue = readValue;
       }
       if (readValue < minValue) 
       {
           //record the maximum sensor value
           minValue = readValue;
       }
   }
   
   // Subtract min from max
   result = ((maxValue - minValue) * 5.0)/1024.0;
      
   return result;
 }


 void systemScane()
 {
  boolean currentelectricityStatus=isElectricityAvailable();
  boolean currentelectricityFlowStatus=isCurrentFlow();
  if((currentelectricityStatus!=prevelectricityStatus)|| (currentelectricityFlowStatus!=prevelectricityFlowStatus))
  {
    prevelectricityStatus=currentelectricityStatus;
    prevelectricityFlowStatus=currentelectricityFlowStatus;
    
   if(currentelectricityStatus)
          {
             if(permanentLightStatus && currentelectricityFlowStatus)
            {
                MQTT.publish(0, 0, 0, msg_id, smart_home_pub_topic, "{\"si_no\":1,\"appliance_id\":\"light1\",\"status_value\":1}" );//ON
                digitalWrite(lightSwitchingPin,permanentLightStatus);
            }
           else if(permanentLightStatus && !currentelectricityFlowStatus)
            {
                MQTT.publish(0, 0, 0, msg_id, smart_home_pub_topic, "{\"si_no\":1,\"appliance_id\":\"light1\",\"status_value\":3}" );//bulb or cable is fused
            }
           else if(!permanentLightStatus && !currentelectricityFlowStatus)
            {
                MQTT.publish(0, 0, 0, msg_id, smart_home_pub_topic, "{\"si_no\":1,\"appliance_id\":\"light1\",\"status_value\":0}" );//OFF
                digitalWrite(lightSwitchingPin,permanentLightStatus);
            }
          }
          else
          {
             MQTT.publish(0, 0, 0, msg_id, smart_home_pub_topic, "{\"si_no\":1,\"appliance_id\":\"light1\",\"status_value\":2}" );//No Electricity
          }
  }
 }

